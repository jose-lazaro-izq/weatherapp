package com.krytek.weatherapp.remote.responses.location_weather


import com.google.gson.annotations.SerializedName

import java.io.Serializable

data class Main(
    val temp: Double,
    val pressure: Double,
    val humidity: Double,
    @SerializedName("temp_min")
    val tempMin: Double,
    @SerializedName("temp_max")
    val tempMax: Double
):Serializable