package com.krytek.weatherapp.di

import com.krytek.weatherapp.viewmodels.GeoLocationViewModel
import com.krytek.weatherapp.viewmodels.WeatherViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { WeatherViewModel(get()) }
    viewModel { GeoLocationViewModel(get()) }

}