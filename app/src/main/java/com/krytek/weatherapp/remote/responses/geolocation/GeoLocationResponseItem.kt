package com.krytek.weatherapp.remote.responses.geolocation


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GeoLocationResponseItem(
    val name: String,
    val lat: Double,
    val lon: Double,
    val country: String
):Serializable