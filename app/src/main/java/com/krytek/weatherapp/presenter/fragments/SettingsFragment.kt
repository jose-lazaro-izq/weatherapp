package com.krytek.weatherapp.presenter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.google.gson.Gson
import com.krytek.weatherapp.R
import com.krytek.weatherapp.databinding.FragmentSettingsBinding
import com.krytek.weatherapp.presenter.base.BaseFragment
import com.krytek.weatherapp.remote.responses.location_weather.WeatherResponse
import com.krytek.weatherapp.utils.*

class SettingsFragment : BaseFragment() {


    private lateinit var btnList: List<ImageView>
    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!
    private lateinit var unitSystem: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentSettingsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
    }

    override fun initObservers() {
        loadCurrentWeather()
    }

    private fun loadCurrentWeather() {
        handleNetworkState(
            showLoading = true,
            title = R.string.updating_information,
            state = weatherViewModel.currentWeatherObserver.observable,
            render = {
                prefs.preference.edit().putFloat(LATITUDE, it.coord.lat.toFloat()).apply()
                prefs.preference.edit().putFloat(LONGITUDE, it.coord.lon.toFloat()).apply()
                prefs.preference.edit().putString(CURRENT_LOCATION, Gson().toJson(it, WeatherResponse::class.java)).apply()
            }
        )
    }

    private fun initData() {
        btnList = listOf(binding.standardSelector, binding.metricSelector, binding.imperialSelector)
        unitSystem = prefs.preference.getString(UNIT_SYSTEM, STANDARD) as String
        setupCurrentSystem()
        setupButtonSelector()
        binding.saveChanges.setOnClickListener {
            prefs.preference.edit().putString(UNIT_SYSTEM, when {
                binding.standardSelector.isSelected -> {
                    STANDARD
                }
                binding.metricSelector.isSelected -> {
                    METRIC
                }
                else -> {
                    IMPERIAL
                }
            }).apply()
            weatherViewModel.getCurrentWeather(prefs.preference.getFloat(LATITUDE, 0f).toDouble(), prefs.preference.getFloat(LONGITUDE, 0f).toDouble())
        }
    }

    private fun setupCurrentSystem() {
        when (unitSystem) {
            STANDARD -> {
                binding.standardSelector.isSelected = true
            }
            METRIC -> {
                binding.metricSelector.isSelected = true
            }
            IMPERIAL -> {
                binding.imperialSelector.isSelected = true
            }
        }
    }

    private fun setupButtonSelector() {
        for (btn in btnList) {
            btn.apply {
                setOnClickListener {
                    isSelected = true
                    btnList.map {
                        if (it != btn)
                            it.isSelected = false
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}