package com.krytek.weatherapp.remote.responses.location_weather


import com.google.gson.annotations.SerializedName

import java.io.Serializable

data class Rain(
    @SerializedName("1h")
    val h: Double?,
    @SerializedName("3h")
    val hrs: Double?,
):Serializable