package com.krytek.weatherapp.repository

import androidx.lifecycle.LiveData
import com.krytek.weatherapp.remote.ApiCallState
import com.krytek.weatherapp.remote.CallUtil
import com.krytek.weatherapp.remote.WeatherApi
import com.krytek.weatherapp.remote.responses.geolocation.GeoLocationResponse
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GeoLocationRepository(private val api: WeatherApi) {

    suspend fun getGeoLocationDataByName(query: String, limit: Int): LiveData<ApiCallState<GeoLocationResponse>> {
        return object : CallUtil<GeoLocationResponse, GeoLocationResponse>() {
            override suspend fun processResponse(response: GeoLocationResponse): GeoLocationResponse {
                return response
            }

            override suspend fun saveCallResults(items: GeoLocationResponse) {
            }

            override fun shouldFetch(data: GeoLocationResponse?): Boolean {
                return true
            }

            override fun useCache(): Boolean {
                return false
            }

            override suspend fun loadFromDb(): GeoLocationResponse {
                TODO("Not yet implemented")
            }

            override suspend fun createCallAsync(): Deferred<GeoLocationResponse> {
                return api.getCityByName(query, limit)
            }
        }.build().asLiveData()
    }

    fun getGeoLocationData(query: String, limit: Int): Flow<GeoLocationResponse> {
        return flow {
           return@flow emit(api.getCityByName(query,limit).await())
        }

    }
}