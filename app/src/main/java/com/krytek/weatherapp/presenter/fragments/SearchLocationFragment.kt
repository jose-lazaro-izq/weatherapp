package com.krytek.weatherapp.presenter.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.krytek.weatherapp.R
import com.krytek.weatherapp.databinding.FragmentSearchLocationBinding
import com.krytek.weatherapp.presenter.base.BaseFragment
import com.krytek.weatherapp.remote.responses.geolocation.GeoLocationResponseItem
import com.krytek.weatherapp.remote.responses.location_weather.WeatherResponse
import com.krytek.weatherapp.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class SearchLocationFragment : BaseFragment() {

    private var _binding: FragmentSearchLocationBinding? = null
    private val binding get() = _binding!!
    private var locationWeather: WeatherResponse? = null
    private var items = mutableListOf<GeoLocationResponseItem>()
    private var places = mutableListOf<String>()
    private var searchAdapter: ArrayAdapter<String>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentSearchLocationBinding.inflate(inflater, container, false)
        val lastKnown = prefs.preference.getString(CURRENT_LOCATION, "") ?: ""
        if (lastKnown.isNotBlank()) {
            locationWeather = Gson().fromJson(lastKnown, WeatherResponse::class.java)
            setInformation(locationWeather!!)
            binding.search.setText(locationWeather!!.name)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
    }

    override fun initObservers() {
        loadGeoLocationInformation()
        loadCurrentWeather()
    }

    private fun loadGeoLocationInformation() {
        handleNetworkState(
            showLoading = false,
            state = geoLocationViewModel.geoLocationObserver.observable,
            render = {
                places.clear()
                it.map { place ->
                    places.add("${place.name} , ${place.country}")
                }
                items.clear()
                items.addAll(it)
                setResults()
            }
        )
    }

    private fun loadCurrentWeather() {
        handleNetworkState(
            showLoading = true,
            title = R.string.retrieving_information,
            state = weatherViewModel.currentWeatherObserver.observable,
            render = {
                setInformation(it)
            }
        )
    }

    private fun setInformation(weatherResponse: WeatherResponse) {
        val unitSystem = prefs.preference.getString(UNIT_SYSTEM, STANDARD)!!
        binding.weatherInformation.date.text = "${getString(R.string.last_update)} ${getCurrentDate(weatherResponse.dt * 1000L)}"
        binding.weatherInformation.locationTitle.text = weatherResponse.name
        binding.weatherInformation.temperature.text = "${weatherResponse.main.temp} ${getTempUnits(unitSystem)}"
        binding.weatherInformation.minTemperature.text = getString(R.string.min_temp, "${weatherResponse.main.tempMin} ${getTempUnits(unitSystem)}")
        binding.weatherInformation.maxTemperature.text = getString(R.string.max_temp, "${weatherResponse.main.tempMax} ${getTempUnits(unitSystem)}")
        binding.weatherInformation.conditionValue.text = weatherResponse.weather[0].description.capitalizeWords()
        binding.weatherInformation.humidityValue.text = "${weatherResponse.main.humidity}"
        binding.weatherInformation.pressureValue.text = "${weatherResponse.main.pressure}"
        binding.weatherInformation.cloudinessValue.text = "${weatherResponse.clouds.all}"
        binding.weatherInformation.windSpeed.text = getString(R.string.wind_speed, if (unitSystem == IMPERIAL) IMPERIAL_SPEED else METRIC_SPEED)
        binding.weatherInformation.windSpeedValue.text = "${weatherResponse.wind.speed}"
        binding.weatherInformation.windDirectionValue.text = "${weatherResponse.wind.deg}"
        binding.weatherInformation.rainContainer.toggleViewVisibility(weatherResponse.rain != null)
        binding.weatherInformation.snowContainer.toggleViewVisibility(weatherResponse.rain != null)
        val image = weatherResponse.weather[0].icon
        Glide.with(requireContext()).load("$BASE_ICON_URL$image@4x.png").into(binding.weatherInformation.weatherIcon)

        weatherResponse.rain?.let {
            it.h?.let { h ->
                binding.weatherInformation.lastHour.apply {
                    visible()
                    text = "$h"
                }
            }
            it.hrs?.let { hrs ->
                binding.weatherInformation.last3hours.apply {
                    visible()
                    text = "$hrs"
                }
            }
        }

        weatherResponse.snow?.let {
            it.h?.let { h ->
                binding.weatherInformation.snowLastHour.apply {
                    visible()
                    text = "$h"
                }
            }
            it.hrs?.let { hrs ->
                binding.weatherInformation.snowLast3hours.apply {
                    visible()
                    text = "$hrs"
                }
            }
        }

        binding.weatherInformation.mainContainer.visible()
    }

    @OptIn(FlowPreview::class)
    private fun initData() {
        binding.search.apply {
            searchPlace(getSearchStateFlow())
            setOnItemClickListener { adapterView, view, i, l ->
                val locationPosition = adapterView.getPositionForView(view)
                weatherViewModel.getCurrentWeather(items[locationPosition].lat, items[locationPosition].lon)
                val imm = requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.applicationWindowToken, 0)
            }
        }
    }


    @FlowPreview
    @OptIn(ExperimentalCoroutinesApi::class)
    private fun searchPlace(place: StateFlow<String>) {
        lifecycleScope.launch {
            place.debounce(250)
                .filter { query ->
                    if (query.isEmpty()) {
                        searchAdapter?.clear()
                        launch(Dispatchers.Main) {
                            locationWeather?.let {
                                setInformation(it)
                            }
                        }
                        return@filter false
                    } else {
                        return@filter true
                    }
                }
                .flatMapLatest { query ->
                    geoLocationViewModel.getGeoLocation(query)
                }
                .flowOn(Dispatchers.Default)
                .collectLatest {
                    places.clear()
                    it.map { place ->
                        if (!items.any { element -> element.name == place.name })
                            places.add("${place.name} , ${place.country}")
                    }
                    items.clear()
                    items.addAll(it)
                    setResults()
                }
        }
    }

    private fun setResults() {
        searchAdapter = ArrayAdapter(requireContext(), R.layout.data_item, R.id.data_title, places)
        binding.search.setAdapter(searchAdapter)
        searchAdapter?.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}