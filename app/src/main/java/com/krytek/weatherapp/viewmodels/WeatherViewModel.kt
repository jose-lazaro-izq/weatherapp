package com.krytek.weatherapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.krytek.weatherapp.remote.ApiCallObserver
import com.krytek.weatherapp.remote.responses.location_weather.WeatherResponse
import com.krytek.weatherapp.repository.WeatherRepository
import kotlinx.coroutines.launch

class WeatherViewModel(private val weatherRepository: WeatherRepository) : ViewModel() {

    val currentWeatherObserver by lazy {
        return@lazy ApiCallObserver<WeatherResponse>()
    }

    fun getCurrentWeather(latitude: Double, longitude: Double) {
        viewModelScope.launch {
            currentWeatherObserver.build(this, weatherRepository.getCurrentWeather(latitude, longitude))
        }
    }
}