package com.krytek.weatherapp.remote.responses.location_weather


import com.google.gson.annotations.SerializedName

data class Sys(
    val type: Int,
    val id: Int,
    val country: String,
    val sunrise: Int,
    val sunset: Int
)