package com.krytek.weatherapp.remote.errors

data class OtherHttpError( val cod: String,
                           val message: String): HttpBaseError()