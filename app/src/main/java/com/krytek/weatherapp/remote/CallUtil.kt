package com.krytek.weatherapp.remote

import android.util.Log
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.krytek.weatherapp.remote.errors.ErrorResponse
import kotlinx.coroutines.*
import retrofit2.HttpException
import kotlin.coroutines.coroutineContext


abstract class CallUtil<ResultType, RequestType> {

    private val result = MutableLiveData<ApiCallState<ResultType>>()
    private val supervisorJob = SupervisorJob()

    suspend fun build(): CallUtil<ResultType, RequestType> {
        withContext(Dispatchers.Main) {
            result.value =
                ApiCallState.loading(null)
        }
        CoroutineScope(coroutineContext).launch(supervisorJob) {
            if (useCache()) {
                val dbResult = loadFromDb()
                Log.d(CallUtil::class.java.name, "Return data from local database")
                setValue(ApiCallState.success(dbResult))
                fetchFromNetwork(false)
            } else
                fetchFromNetwork()
        }
        return this
    }

    fun asLiveData() = result as LiveData<ApiCallState<ResultType>>

    private suspend fun fetchFromNetwork(notify: Boolean = true) {
        try {
            Log.d(CallUtil::class.java.name, "Fetch data from network")
            if (notify)
                setValue(ApiCallState.loading())
            val apiResponse = createCallAsync().await()
            Log.e(CallUtil::class.java.name, "DomainData fetched from network")
            saveCallResults(processResponse(apiResponse))
            if (notify && useCache())
                setValue(ApiCallState.success(loadFromDb()))
            else
                setValue(ApiCallState.success(processResponse(apiResponse)))
        } catch (e: Exception) {
            if (e is HttpException) {
                setValue(ApiCallState.apiError(Gson().fromJson(e.response()!!.errorBody()!!.string(), ErrorResponse::class.java), null))
            } else {
                if (notify && useCache())
                    setValue(ApiCallState.error(e, loadFromDb()))
                else
                    setValue(ApiCallState.error(e, null))
            }
            Log.e("CallUtil", "An error happened: $e")
            e.printStackTrace()
        }
    }

    @MainThread
    private fun setValue(newValue: ApiCallState<ResultType>) {
        Log.d(CallUtil::class.java.name, "Resource: $newValue")
        if (result.value != newValue) result.postValue(newValue)
    }

    @WorkerThread
    protected abstract suspend fun processResponse(response: RequestType): ResultType

    @WorkerThread
    protected abstract suspend fun saveCallResults(items: ResultType)

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun useCache(): Boolean

    @MainThread
    protected abstract suspend fun loadFromDb(): ResultType

    @MainThread
    protected abstract suspend fun createCallAsync(): Deferred<RequestType>
}