package com.krytek.weatherapp.presenter.activities

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.krytek.weatherapp.R
import com.krytek.weatherapp.databinding.ActivityMainBinding
import com.krytek.weatherapp.presenter.base.BaseActivity
import com.krytek.weatherapp.utils.PERMISSIONS_ACCEPTED
import com.krytek.weatherapp.utils.isLocationEnabled
import com.krytek.weatherapp.utils.showAlertDialog

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration
    private var lastDestinationId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_WeatherApp)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setSupportActionBar(binding.toolbar)
        setContentView(binding.root)
        lastDestinationId = R.id.my_location
    }

    private fun initData() {
        val navigation: BottomNavigationView = binding.bottomBar
        navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(R.id.my_location, R.id.search_location, R.id.settings))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navigation.setupWithNavController(navController)


    }

    override fun onResume() {
        super.onResume()
        initData()
    }


    override fun onNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }


}