package com.krytek.weatherapp.remote.responses.geolocation


import com.google.gson.annotations.SerializedName

class GeoLocationResponse : ArrayList<GeoLocationResponseItem>()