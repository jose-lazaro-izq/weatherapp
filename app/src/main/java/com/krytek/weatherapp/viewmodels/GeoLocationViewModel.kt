package com.krytek.weatherapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.krytek.weatherapp.remote.ApiCallObserver
import com.krytek.weatherapp.remote.responses.geolocation.GeoLocationResponse
import com.krytek.weatherapp.repository.GeoLocationRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class GeoLocationViewModel(private val geoLocationRepository: GeoLocationRepository) : ViewModel() {

    val geoLocationObserver by lazy {
        return@lazy ApiCallObserver<GeoLocationResponse>()
    }

    fun getGeoLocationInformation(query: String, limit: Int = 5) {
        viewModelScope.launch {
            geoLocationObserver.build(this, geoLocationRepository.getGeoLocationDataByName(query, limit))
        }
    }

    fun getGeoLocation(query: String, limit: Int = 3): Flow<GeoLocationResponse> = geoLocationRepository.getGeoLocationData(query, limit)

}