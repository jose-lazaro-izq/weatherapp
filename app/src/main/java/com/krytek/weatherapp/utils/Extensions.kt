package com.krytek.weatherapp.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.LocationManager
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.SpannableString
import android.text.TextWatcher
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import com.krytek.weatherapp.R
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.text.DateFormatSymbols
import java.util.*
import java.util.concurrent.Executors


fun showAlertDialog(
    context: Context,
    title: String? = context.getString(R.string.error),
    msg: String? = "",
    negative: Boolean = false,
    positiveTitle: String? = context.getString(R.string.ok),
    negativeTitle: String? = context.getString(R.string.cancel),
    action: (() -> Unit)? = null,
    negativeAction: (() -> Unit)? = null,
    styled: Boolean = false,
) {
    val builder = AlertDialog.Builder(context)
    builder.setTitle(title)
    builder.setMessage(msg)
    builder.setCancelable(false)

    val positiveBtnTitle = SpannableString(positiveTitle)
    val negativeBtnTitle = SpannableString(negativeTitle)

    if (styled) {
        positiveBtnTitle.setSpan(
            ForegroundColorSpan(context.getColor(R.color.colorPrimary)),
            0,
            positiveBtnTitle.length,
            SpannableString.SPAN_INCLUSIVE_INCLUSIVE
        )
        negativeBtnTitle.setSpan(
            ForegroundColorSpan(context.getColor(R.color.colorPrimaryDark)),
            0,
            positiveBtnTitle.length,
            SpannableString.SPAN_INCLUSIVE_INCLUSIVE
        )
    }

    builder.setPositiveButton(positiveBtnTitle) { dialog, i ->
        action?.invoke()
        dialog.dismiss()
    }
    if (negative)
        builder.setNegativeButton(negativeBtnTitle) { dialog, i ->
            negativeAction?.invoke()
            dialog.dismiss()
        }
    builder.show()
}

fun getTempUnits(unitSystem:String): String {
    return when (unitSystem) {
        STANDARD -> {
            STANDARD_TEMP_SYMBOL
        }
        METRIC -> {
            METRIC_TEMP_SYMBOL
        }
        else -> {
            IMPERIAL_TEMP_SYMBOL
        }
    }
}


fun isLocationEnabled(context: Context?, showDialog: Boolean, action: (() -> Unit)?=null, negativeAction: (() -> Unit)?=null): Boolean {
    if (context == null) {
        return false
    }
    var gpsNetworkEnabled = false
    try {
        val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        gpsNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    if (showDialog && !gpsNetworkEnabled) {
        showDialogEnableGPS(context, action,negativeAction)
    }
    return gpsNetworkEnabled
}

fun showDialogEnableGPS(context: Context, action: (() -> Unit)?, negativeAction: (() -> Unit)?) {
    val dialog: android.app.AlertDialog.Builder = android.app.AlertDialog.Builder(context)
    dialog.setCancelable(false)
    dialog.setMessage(context.getString(R.string.generic_gps_disabled))
    dialog.setPositiveButton(context.getString(R.string.text_gps_on)) { paramDialogInterface, paramInt -> action?.invoke() }
    dialog.setNegativeButton(R.string.cancel) { _, _ ->
        negativeAction?.invoke()
    }
    dialog.show()}



fun View.hide() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}
fun ImageView.loadImage(url: String) = run {
    val executor = Executors.newSingleThreadExecutor()

    val handler = Handler(Looper.getMainLooper())

    var image: Bitmap? = null

    executor.execute {
        try {
            val stream = java.net.URL(url).openStream()
            image = BitmapFactory.decodeStream(stream)
            handler.post {
                this.setImageBitmap(image)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

fun getCurrentDate(milliseconds: Long): CharSequence {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = milliseconds
    val day = calendar.get(Calendar.DAY_OF_MONTH)
    val month = DateFormatSymbols(Locale.getDefault()).months[calendar.get(Calendar.MONTH)]
    val weekDay = DateFormatSymbols(Locale.getDefault()).weekdays[calendar.get(Calendar.DAY_OF_WEEK)]
    val hours = calendar.get(Calendar.HOUR_OF_DAY)
    val minutes = calendar.get(Calendar.MINUTE)
    return "$weekDay, $month $day $hours:${if(minutes<10) "0$minutes" else minutes}"
}

fun AutoCompleteTextView.getSearchStateFlow(): StateFlow<String> {
    val stateFlow = MutableStateFlow("")

    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(
            s: CharSequence?,
            start: Int,
            count: Int,
            after: Int,
        ) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            s?.let {
                stateFlow.value = s.toString()
                return
            }
            stateFlow.value = ""
        }
    })
    return stateFlow
}



fun View.toggleViewVisibility(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun String.capitalizeWords(): String = split(" ").joinToString(" ") { it.lowercase().replaceFirst(it.first(), it.first().uppercaseChar()) }