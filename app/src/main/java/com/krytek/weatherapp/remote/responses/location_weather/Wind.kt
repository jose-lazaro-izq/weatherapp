package com.krytek.weatherapp.remote.responses.location_weather


import com.google.gson.annotations.SerializedName

import java.io.Serializable

data class Wind(
    val speed: Double,
    val deg: Int
):Serializable