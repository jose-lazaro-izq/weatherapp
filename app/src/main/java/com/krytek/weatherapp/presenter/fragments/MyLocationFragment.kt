package com.krytek.weatherapp.presenter.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import com.bumptech.glide.Glide
import com.fondesa.kpermissions.allGranted
import com.fondesa.kpermissions.extension.permissionsBuilder
import com.fondesa.kpermissions.extension.send
import com.google.gson.Gson
import com.krytek.weatherapp.R
import com.krytek.weatherapp.databinding.FragmentMyLocationBinding
import com.krytek.weatherapp.presenter.base.BaseFragment
import com.krytek.weatherapp.remote.responses.location_weather.WeatherResponse
import com.krytek.weatherapp.utils.*
import kotlinx.coroutines.launch
import java.util.*

class MyLocationFragment : BaseFragment(), LocationListener {
    private var locationProvider: Location? = null
    private var locationManager: LocationManager?=null
    private var requestedOnce = false
    private var permissionAccepted = false
    private var isShowingDialog = false

    private var _binding: FragmentMyLocationBinding? = null
    private val binding get() = _binding!!

    private val openPhoneSettings = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        forcePermissionAccepted()
    }
    private val openLocationSettings = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        forcePermissionAccepted()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentMyLocationBinding.inflate(inflater, container, false)
        binding.gotoServiceContainer.hide()
        if (isLocationEnabled(requireContext(), true, action = { openLocationSettings(requireContext()) }, negativeAction = { showServiceContainer() })) {
            if (!requestedOnce) {
                requestedOnce = true
                forcePermissionAccepted()
            } else {
                if (permissionAccepted) {
                    weatherViewModel.getCurrentWeather(locationProvider!!.latitude, locationProvider!!.longitude)
                } else {
                    binding.gotoSettingsContainer.visible()
                }
            }
        } else {
            binding.gotoServiceContainer.visible()
        }
        return binding.root
    }


    override fun initObservers() {
        loadCurrentWeather()
    }

    private fun loadCurrentWeather() {
        handleNetworkState(
            showLoading = false,
            state = weatherViewModel.currentWeatherObserver.observable,
            render = {
                setInformation(it)
                prefs.preference.edit().putFloat(LATITUDE, it.coord.lat.toFloat()).apply()
                prefs.preference.edit().putFloat(LONGITUDE, it.coord.lon.toFloat()).apply()
                prefs.preference.edit().putString(CURRENT_LOCATION, Gson().toJson(it, WeatherResponse::class.java)).apply()
            }
        )
    }

    private fun setInformation(weatherResponse: WeatherResponse) {
        val unitSystem = prefs.preference.getString(UNIT_SYSTEM, STANDARD)!!
        binding.weatherInformation.locationTitle.text = weatherResponse.name
        binding.weatherInformation.date.text = "${getString(R.string.last_update)} ${getCurrentDate(weatherResponse.dt * 1000L)}"
        binding.weatherInformation.temperature.text = "${weatherResponse.main.temp} ${getTempUnits(unitSystem)}"
        binding.weatherInformation.minTemperature.text = getString(R.string.min_temp, "${weatherResponse.main.tempMin} ${getTempUnits(unitSystem)}")
        binding.weatherInformation.maxTemperature.text = getString(R.string.max_temp, "${weatherResponse.main.tempMax} ${getTempUnits(unitSystem)}")
        binding.weatherInformation.conditionValue.text = weatherResponse.weather[0].description.capitalizeWords()
        binding.weatherInformation.humidityValue.text = "${weatherResponse.main.humidity}"
        binding.weatherInformation.pressureValue.text = "${weatherResponse.main.pressure}"
        binding.weatherInformation.cloudinessValue.text = "${weatherResponse.clouds.all}"
        binding.weatherInformation.windSpeed.text = getString(R.string.wind_speed, if (unitSystem == IMPERIAL) IMPERIAL_SPEED else METRIC_SPEED)
        binding.weatherInformation.windSpeedValue.text = "${weatherResponse.wind.speed}"
        binding.weatherInformation.windDirectionValue.text = "${weatherResponse.wind.deg}"
        binding.weatherInformation.rainContainer.toggleViewVisibility(weatherResponse.rain != null)
        binding.weatherInformation.snowContainer.toggleViewVisibility(weatherResponse.rain != null)
        val image = weatherResponse.weather[0].icon
        Glide.with(requireContext()).load("$BASE_ICON_URL$image@4x.png").into(binding.weatherInformation.weatherIcon)

        weatherResponse.rain?.let {
            it.h?.let { h ->
                binding.weatherInformation.lastHour.apply {
                    visible()
                    text = "$h"
                }
            }
            it.hrs?.let { hrs ->
                binding.weatherInformation.last3hours.apply {
                    visible()
                    text = "$hrs"
                }
            }
        }

        weatherResponse.snow?.let {
            it.h?.let { h ->
                binding.weatherInformation.snowLastHour.apply {
                    visible()
                    text = "$h"
                }
            }
            it.hrs?.let { hrs ->
                binding.weatherInformation.snowLast3hours.apply {
                    visible()
                    text = "$hrs"
                }
            }
        }

        binding.weatherInformation.mainContainer.visible()
    }

    private fun initData() {
        binding.gotoServiceContainer.setOnClickListener {
            openLocationSettings(requireContext())
        }
        binding.gotoSettingsContainer.setOnClickListener {
            openSettings()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onResume() {
        super.onResume()
        initData()
    }


    private fun showServiceContainer() {
        binding.gotoServiceContainer.visible()
    }

    private fun openLocationSettings(context: Context?) {
        if (context == null) {
            return
        }
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        openLocationSettings.launch(intent)
    }

    private fun forcePermissionAccepted() {
        permissionsBuilder(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION).build().send { result ->
            if (result.allGranted()) {
                permissionAccepted = true
                prefs.preference.edit().putBoolean(PERMISSIONS_ACCEPTED, true).apply()
                if (isLocationEnabled(requireContext(), true, action = { openLocationSettings(requireContext()) }, negativeAction = { showServiceContainer() })) {
                    binding.gotoServiceContainer.hide()
                    binding.gotoSettingsContainer.hide()
                    showLoading(R.string.retrieving_information)
                    // showLoading(R.string.retrieving_information)
                    if (locationProvider == null) {
                        initLocation()
                    }else{
                        weatherViewModel.getCurrentWeather(locationProvider!!.latitude, locationProvider!!.longitude)
                    }
                } else {
                    binding.gotoServiceContainer.visible()
                    binding.gotoSettingsContainer.hide()
                }
            } else {
                if (!isShowingDialog) {
                    isShowingDialog = true
                    showAlertDialog(requireActivity(), getString(R.string.permission_error), getString(R.string.permission_denied),
                        positiveTitle = getString(R.string.grant_permission),
                        negative = true,
                        action = {
                            isShowingDialog = false
                            openSettings()
                        },
                        negativeAction = {
                            isShowingDialog = false
                            binding.gotoSettingsContainer.visible()
                        })
                }
            }
        }
    }


    @SuppressLint("MissingPermission")
    private fun initLocation() = launch {
        binding.gotoServiceContainer.hide()

        locationManager = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager?.requestLocationUpdates(LocationManager.GPS_PROVIDER, HOUR2MILLI, MINIMUM_DISTANCE, this@MyLocationFragment)
    }

    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", requireContext().packageName, null)
        intent.data = uri
        openPhoneSettings.launch(intent)
    }

    @SuppressLint("MissingPermission")
    override fun onLocationChanged(p0: Location) {
        locationProvider = locationManager?.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        if (locationProvider != null) {
            weatherViewModel.getCurrentWeather(locationProvider!!.latitude, locationProvider!!.longitude)
        }
    }

}