package com.krytek.weatherapp.remote.responses.location_weather


import com.google.gson.annotations.SerializedName

import java.io.Serializable

data class Coord(
    val lat: Double,
    val lon: Double
):Serializable