package com.krytek.weatherapp.di

import com.krytek.weatherapp.utils.PreferenceManager
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val appModule  = module{
    single { PreferenceManager(androidApplication()) }
}