package com.krytek.weatherapp.remote.errors

data class HttpError500( val cod: String,
                         val message: String) : HttpBaseError()