package com.krytek.weatherapp.remote.errors


import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    val cod: String,
    val message: String
):HttpBaseError()