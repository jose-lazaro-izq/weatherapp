package com.krytek.weatherapp.remote

import com.krytek.weatherapp.remote.responses.geolocation.GeoLocationResponse
import com.krytek.weatherapp.remote.responses.location_weather.WeatherResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("data/2.5/weather")
    fun getCurrentWeather(@Query("lat") latitude: Double, @Query("lon") longitude: Double): Deferred<WeatherResponse>

    @GET("geo/1.0/direct")
    fun getCityByName(@Query("q") query: String, @Query("limit") limit: Int): Deferred<GeoLocationResponse>



}