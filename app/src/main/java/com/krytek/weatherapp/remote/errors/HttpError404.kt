package com.krytek.weatherapp.remote.errors

data class HttpError404( val cod: String,
                         val message: String) : HttpBaseError()