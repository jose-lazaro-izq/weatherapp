package com.krytek.weatherapp.repository

import androidx.lifecycle.LiveData
import com.krytek.weatherapp.remote.ApiCallState
import com.krytek.weatherapp.remote.CallUtil
import com.krytek.weatherapp.remote.WeatherApi
import com.krytek.weatherapp.remote.responses.location_weather.WeatherResponse
import com.krytek.weatherapp.utils.APP_ID
import kotlinx.coroutines.Deferred

class WeatherRepository(private val api: WeatherApi) {
    suspend fun getCurrentWeather(latitude: Double, longitude: Double): LiveData<ApiCallState<WeatherResponse>> {
        return object : CallUtil<WeatherResponse, WeatherResponse>() {
            override suspend fun processResponse(response: WeatherResponse): WeatherResponse {
                return response
            }

            override suspend fun saveCallResults(items: WeatherResponse) {
            }

            override fun shouldFetch(data: WeatherResponse?): Boolean {
                return true
            }

            override fun useCache(): Boolean {
                return false
            }

            override suspend fun loadFromDb(): WeatherResponse {
                TODO("Not yet implemented")
            }

            override suspend fun createCallAsync(): Deferred<WeatherResponse> {
                return api.getCurrentWeather(latitude, longitude)
            }
        }.build().asLiveData()
    }


}