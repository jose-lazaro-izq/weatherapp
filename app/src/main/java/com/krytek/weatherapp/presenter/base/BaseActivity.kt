package com.krytek.weatherapp.presenter.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.krytek.weatherapp.R
import com.krytek.weatherapp.databinding.LoadingDialogLayoutBinding
import com.krytek.weatherapp.remote.ApiCallState
import com.krytek.weatherapp.remote.errors.ErrorResponse
import com.krytek.weatherapp.remote.errors.HttpError404
import com.krytek.weatherapp.remote.errors.HttpError500
import com.krytek.weatherapp.remote.errors.OtherHttpError
import com.krytek.weatherapp.utils.PreferenceManager
import com.krytek.weatherapp.utils.showAlertDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.Job
import org.koin.android.ext.android.inject
import kotlin.coroutines.CoroutineContext

open class BaseActivity : AppCompatActivity(), CoroutineScope {
    private lateinit var job: Job
    lateinit var dialog: MaterialDialog
    val prefs :PreferenceManager by inject()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = MaterialDialog(this)
        initObservers()
        job = Job()
    }

    override fun onResume() {
        super.onResume()
        job = Job()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }
    open fun initObservers() {}

    @OptIn(InternalCoroutinesApi::class)
    fun <T> handleNetworkState(
        showLoading: Boolean = true, state: LiveData<ApiCallState<T>>,
        render: ((data: T) -> Unit)? = null,
        error: ((error: String) -> Unit)? = null,
        loading: (() -> Unit)? = null,
        dismiss: (() -> Unit)? = null,
        title: Int? = null,
    ) {
        state.observe(this, Observer {
            when (it.status) {
                ApiCallState.Status.LOADING -> {
                    if (loading == null && showLoading)
                        showLoading(title ?: R.string.loading)
                    else
                        loading?.let { it1 -> it1() }
                }
                ApiCallState.Status.SUCCESS -> {
                    dialog.dismiss()
                    render?.apply {
                        this(it.data!!)
                    }
                }
                else -> {
                    dialog.dismiss()
                    when {
                        it.apiError != null ->
                            showAlertDialog(this, msg =
                                when (it.apiError) {
                                    is HttpError500 -> {
                                        it.apiError.message
                                    }

                                    is HttpError404 -> {
                                        it.apiError.message
                                    }
                                    else -> {
                                        (it.apiError as ErrorResponse).message
                                    }
                                })
                        error == null -> showAlertDialog(
                            this,
                            "Error",
                            (it).error!!.localizedMessage
                        )
                        else -> error.invoke(it.error!!.localizedMessage)
                    }
                }
            }
        })
    }

    fun showLoading(content: Int) {
        dialog?.let {
            if (it.isShowing)
                it.dismiss()
        }

        val dialogBinding = LoadingDialogLayoutBinding.inflate(layoutInflater)

        dialog = MaterialDialog(this)
        dialog.apply {
            customView(view = dialogBinding.root)
            cancelable(false)
            cancelOnTouchOutside(false)
        }
        dialog.getCustomView().apply {
            dialogBinding.dialogMessage.setText(content)
        }

        dialog.show()
    }
}