package com.krytek.weatherapp.utils

const val BASE_URL = "https://api.openweathermap.org/"
//Example http://openweathermap.org/img/wn/10d@2x.png
const val BASE_ICON_URL = "https://openweathermap.org/img/wn/"
//Sample parameters https://api.openweathermap.org/geo/1.0/direct?q=london&limit=1&appid=f8cb8ecbcc0cc60f06920c495f3d4c8f"
const val BASE_GEOLOCATION = "https://api.openweathermap.org/geo/1.0/direct"
const val APP_ID = "f8cb8ecbcc0cc60f06920c495f3d4c8f"
const val ERROR_EXAMPLE = "https://api.openweathermap.org/data/2.5/weather?lat=&lon={lon}&appid=f8cb8ecbcc0cc60f06920c495f3d4c8f"

const val PERMISSIONS_ACCEPTED = "PERMISSION_ACCEPTED"
const val UNIT_SYSTEM = "UNIT_SYSTEM"
const val LATITUDE= "LATITUDE"
const val LONGITUDE = "LONGITUDE"
const val CURRENT_LOCATION = "CURRENT_LOCATION"
const val LOCATION_PERMISSION = 21
const val MINIMUM_DISTANCE = 10f
const val GPS_ENABLE = 5353
const val HOUR2MILLI = 3600000L
const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

const val STANDARD=  "standard"
const val METRIC=  "metric"
const val IMPERIAL=  "imperial"

const val STANDARD_TEMP_SYMBOL = "K"
const val METRIC_TEMP_SYMBOL = "°C"
const val IMPERIAL_TEMP_SYMBOL = "°F"
const val IMPERIAL_SPEED = "miles/hour"
const val METRIC_SPEED = "meter/sec"
