package com.krytek.weatherapp.remote.responses.location_weather


import com.google.gson.annotations.SerializedName

import java.io.Serializable

data class WeatherResponse(
    val id: Int,
    val dt: Int,
    val name: String,
    val coord: Coord,
    val main: Main,
    val wind: Wind,
    val weather: List<Weather>,
    val clouds: Clouds,
    val sys: Sys,
    val timezone: Int,
    val snow: Snow?,
    val rain: Rain?
):Serializable