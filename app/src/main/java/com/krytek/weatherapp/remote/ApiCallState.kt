package com.krytek.weatherapp.remote

import com.krytek.weatherapp.remote.errors.HttpBaseError

// import com.module.remote.responses.terms.Description

data class ApiCallState<out T>(val status: Status, val data: T?, val apiError: HttpBaseError?, val error: Throwable?) {
    companion object {
        fun <T> success(data: T?): ApiCallState<T> {
            return ApiCallState(
                    Status.SUCCESS,
                    data,
                    null
                    , null
            )
        }

        fun <T> error(error: Throwable, data: T?): ApiCallState<T> {
            return ApiCallState(
                    Status.ERROR,
                    data,
                    null,
                    error
            )
        }

        fun <T> apiError(error: HttpBaseError, data: T?): ApiCallState<T> {
            return ApiCallState(
                    Status.ERROR,
                    data,
                    error,
                    null
            )
        }

        fun <T> loading(data: T? = null): ApiCallState<T> {
            return ApiCallState(
                    Status.LOADING,
                    data,
                    null,
                    null
            )
        }
    }

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }
}