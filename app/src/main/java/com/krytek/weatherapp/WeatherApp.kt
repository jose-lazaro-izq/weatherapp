package com.krytek.weatherapp

import androidx.multidex.MultiDexApplication
import com.blankj.utilcode.util.Utils
import com.krytek.weatherapp.di.appModule
import com.krytek.weatherapp.di.remoteModule
import com.krytek.weatherapp.di.repositoryModule
import com.krytek.weatherapp.di.viewModelModule
import com.krytek.weatherapp.utils.BASE_URL
import com.krytek.weatherapp.utils.PreferenceManager
import com.krytek.weatherapp.utils.STANDARD
import com.krytek.weatherapp.utils.UNIT_SYSTEM
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherApp : MultiDexApplication() {
    val prefs: PreferenceManager by lazy {
        return@lazy get<PreferenceManager>()
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@WeatherApp)
            modules(appModule, remoteModule(BASE_URL), repositoryModule, viewModelModule)
        }

        Utils.init(this)
        preferenceManager = prefs
        prefs.preference.edit().putString(UNIT_SYSTEM, STANDARD).apply()
    }

    companion object {
        lateinit var preferenceManager: PreferenceManager
    }
}