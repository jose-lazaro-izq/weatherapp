package com.krytek.weatherapp.remote.responses.location_weather


import com.google.gson.annotations.SerializedName

import java.io.Serializable

data class Weather(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
):Serializable