
object Version {
    const val kot_version = "1.5.20"
    const val kot_corroutine = "1.4.2"
    const val supportLib = "28.0.0"
    const val constraintJet="2.1.3"
    const val joda = "2.9.9.3"
    const val koin = "3.1.5"
    const val anko = "0.10.5"
    const val room="2.2.6"
    const val retrofit = "2.9.0"
    const val gson = "2.8.6"
    const val retrofitCoroutine = "0.9.2"
    const val retrofitConverter = "2.9.0"
    const val retrofitInterceptor = "4.9.0"
    const val retrofitAnnotation = "4.0-b33"
    const val gradleBuild= "7.0.2"
    const val jetLifecycle="2.2.0"
}

object Depend {
   const val multidex="androidx.multidex:multidex:2.0.1"
    const val gradleBuild="com.android.tools.build:gradle:${Version.gradleBuild}"
    const val kotlinVersion="org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.kot_version}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Version.kot_version}"
    const val kotlinCoroutine = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Version.kot_corroutine}"

    const val appCompact = "androidx.appcompat:appcompat:1.0.2"
    const val design = "com.google.android.material:material:1.3.0-alpha04"
    const val v4 = "androidx.legacy:legacy-support-v4:1.0.0"
    const val v13 = "com.android.support:support-v13:${Version.supportLib}"
    const val customTabs = "androidx.browser:browser:1.3.0"
    const val recycler = "androidx.recyclerview:recyclerview:1.1.0"
    const val constraint = "androidx.constraintlayout:constraintlayout:${Version.constraintJet}"

    const val firebaseCrash= "com.google.firebase:firebase-crashlytics-ktx"
    const val firebaseAnalitycs= "com.google.firebase:firebase-analytics-ktx"
    const val fireBaseDatabase= "com.google.firebase:firebase-database-ktx"
    const val fireBaseMessaging= "com.google.firebase:firebase-messaging-ktx"
    const val fireBaseAuth= "com.google.firebase:firebase-auth-ktx"

    const val joda = "net.danlew:android.joda:${Version.joda}"
    const val koinCore = "io.insert-koin:koin-android:${Version.koin}"

    const val anko = "org.jetbrains.anko:anko-commons:${Version.anko}"

    const val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Version.jetLifecycle}"
    const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Version.jetLifecycle}"
    const val lifecycleViewLivedata = "androidx.lifecycle:lifecycle-livedata-ktx:${Version.jetLifecycle}"
    const val workManager = "androidx.work:work-runtime-ktx${Version.jetLifecycle}"

    const val roomRuntime = "androidx.room:room-runtime:${Version.room}"
    const val roomCompiler = "androidx.room:room-compiler:${Version.room}"
    const val roomKtx = "androidx.room:room-ktx:${Version.room}"

    const val lifecycleRuntime = "androidx.lifecycle:lifecycle-runtime-ktx:${Version.jetLifecycle}"
    const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Version.jetLifecycle}"

    const val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofit}"
    const val retrofitGson = "com.google.code.gson:gson:${Version.gson}"
    const val retrofitConverter = "com.squareup.retrofit2:converter-gson:${Version.retrofitConverter}"
    const val retrofitCoroutine = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${Version.retrofitCoroutine}"
    const val retrofitLoginInterceptor = "com.squareup.okhttp3:logging-interceptor:${Version.retrofitInterceptor}"
    const val retrofitAnnotations = "org.glassfish.main:javax.annotation:${Version.retrofitAnnotation}"

    const val util = "com.blankj:utilcodex:1.31.0"
    const val picasso = "com.squareup.picasso:picasso:2.5.2"

    //DatePicker
    const val datePicker = "com.wdullaer:materialdatetimepicker:4.2.3"
    const val circleImageView = "de.hdodenhof:circleimageview:3.1.0"
    //ImagePicker
    const val imagePicker = "com.github.jkwiecien:EasyImage:3.0.3"
    //permissionlib
    const val permission = "com.github.fondesa:kpermissions:3.1.3"
    const val materialDialogs = "com.afollestad.material-dialogs:core:3.3.0"
    const val flexBoxLayout = "com.google.android:flexbox:1.1.1"
    const val materialRangeBar = "com.appyvet:materialrangebar:1.4.8"

    const val compressImage="id.zelory:compressor:3.0.0"
    const val lottie="com.airbnb.android:lottie:2.8.0"

}