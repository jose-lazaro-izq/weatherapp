package com.krytek.weatherapp.di

import com.krytek.weatherapp.repository.GeoLocationRepository
import com.krytek.weatherapp.repository.WeatherRepository
import com.krytek.weatherapp.viewmodels.WeatherViewModel
import org.koin.dsl.module

val repositoryModule = module {
    single { WeatherRepository(get()) }
    single { GeoLocationRepository(get()) }
}