package com.krytek.weatherapp.utils

import android.content.Context
import android.content.SharedPreferences

class PreferenceManager {
    var preference: SharedPreferences

    constructor(context: Context) {
        preference = context.getSharedPreferences("PREF", Context.MODE_PRIVATE)
    }
}